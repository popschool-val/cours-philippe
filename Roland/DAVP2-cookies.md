# Droits d’auteur, respect de la vie privée : obligation des cookies, déclaration CNIL

Philippe Pary, 2017© CC-BY-SA
Pour PopSchool Lens. http://pop.eu.com

## Simple

Vous faites pas chier, tout est sur <https://www.cnil.fr>. Vous pouvez dormir durant ce cours, tant que vous oubliez pas que tout est sur <https://www.cnil.fr>. Mais vraiment, limite vous pouvez aller vous rouler un joint, mais attention aux pertes de mémoire, faudrait pas oublier <https://www.cnil.fr>

<https://www.cnil.fr>

Merci et à la semaine prochaine !

…

Si vous êtes toujours là …

## Obligations européennes sur les cookies

Vous pouvez consulter l’excellente page de la CNIL sur le sujet:
<https://www.cnil.fr/fr/cookies-traceurs-que-dit-la-loi>

Vous avez obligation si:

* Vous avez un google Analytics ou tout outil similaire (mais un [Piwik](http://www.piwik.org/) ça peut passer)
* Vous tenter de collecter des informations personnalisées sur vos visiteurs
* Vous utilisez des services tiers (boutons réseaux sociaux & cie)

Vous devez:

* Obtenir l’accord de l’utilisateur
* Ça doit être visible sur toutes les pages tant qu’il n’a pas validé
* Les options doivent être désactivées tant qu’il n’a pas validé

Là encore le site de la CNIL est un modèle du genre. Au delà du refus, il présente des options activables à la carte. Lisez le code source, pour rire un peu.

Tout ça va évoluer grâce à la commission européenne qui reportera les obligations sur les navigateurs webs. Tweet related : <https://twitter.com/ubiquitic/status/820418329185841156>

## Déclaration CNIL

Dès que vous commencez à:

* Gérer des comptes utilisateurs
* Enregistrer des données de vos visiteurs

Vous devez:

* **faire une déclaration de fichier à la CNIL**

(joie)

C’est facile comme tout, et c’est *gratuit*

<https://www.cnil.fr/fr/declarer-un-fichier>

On va déclarer un fichier fictif ensemble, juste pour démystifier le truc.

Répondez simplement, ne cherchez pas compliqué quand c’est simple.

Par exemple: *finalité: permettre aux utilisateurs du site web d’enregistrer leurs préférences pour être alertés par mail quand un nouvel article correspondant à leurs préférences est publié* (pour un site de vente en ligne)



