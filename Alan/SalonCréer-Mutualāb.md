# Journée à Lille le 14/09

Accompagnant: Philippe Pary (0688606945)

**Cette journée est une journée de cours normale, toute absence doit être annoncée et justifiée**

*En cas de soucis financier pour la sortie, contactez Brahim*

## Salon créer

Rendez-vous à 10H à l’intérieur du Grand Palais, avant la zone de fouilles

Nous irons voir les sociétés de portage salarial ainsi que les structures d’accompagnement type BGE et les financeurs (BPI)

## Mutualāb

Rendez-vous à 14H dans le couloir d’entrée du Mutualāb

* Visite du lieu réalisée par Florette
* Cours sur la création d’entreprise de 15H à 17H30


