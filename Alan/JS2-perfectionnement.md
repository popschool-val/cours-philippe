# Perfectionnement JavaScript

Philippe Pary, 2016 dans le cadre de POPSchool Valenciennes. http://pop.eu.com  
CC-BY-SA

## Objectifs du cours

* Comprendre les fonction anonymes
* Comprendre les objects et l’héritage en JavaScript
* Comprendre la gestion d’exceptions
* Savoir utiliser le localstorage
* Savoir réaliser une requête XMLHttpRequest (XHR) sur une API, exploiter le résultat

## Fonctions anonymes ?

La programmation séquentielle : on fait les choses l’une à la suite de l’autre. C’est instinctif, c’est ce que vous faites.

Mais il serait inacceptable d’empêcher quelqu’un de cliquer sur un bouton au prétexte d’un code un peu long à exécuter (appel à une API par exemple). *Imagine-t-on facebook freezer le temps que se charge la suite du flux d’actualités ?*  
On entre dans la programmation événementielle : le langage lance des évènements qui peuvent se résoudre de manière *asynchrone*.


