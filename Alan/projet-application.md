Création d'une application pour gérer les présences / absences des promos
======

L'application devra gérer la connexion d'un ou plusieurs *admin* / *user*.

Aucune connexion pour un visiteur non identifié.

La gestion des abscences devra prendre en compte les matins et les apres midi.

L'application pourra gérer différentes promotions en même temps.

L'admin
===
L'admin pourra créer des comptes *user* et les supprimer.

L'admin pourra si possible imprimer des PDF et faire des exports CSV.

L'admin peut éditer si besoin un état. 

Stats
====

On pourra afficher des vues statistiques par jour / semaine / mois / 6 mois 

L'utilisateur
=====

L'utilisateur ne pourra que saisir les présences/absences, il ne pourra pas afficher exporter les statistiques.

Gestion :
====
L'admin pourra saisir les informations les effacer, les traiter sous forme de stat

L'administreur ou l'utilisateur devra pourvoir télécharger le justificatif d'absence




Réalisation du projet 
===

- *Livraison pour le* vendredi 16 septembre.  **SANS BUG ET FONCTIONNEL**
- *code basé sur* : php
- *base de données* : Mysql





Évolution du cdc 
===

Suite à une remarque pertinente de Victor, 

L'application devra permettre à un étudiant d'y poster un message avec une date afin de prévenir d'une absence planifiée.

Celui ci sera directement posté sur le chan de popschool de la promo d'appartenance nommé Absences

Et sera automatiquement enregistré à la date choisie pour l'éléve choisi en commentaire




