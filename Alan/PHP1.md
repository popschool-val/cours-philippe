# PHP, niveau 1

Ce cours va introduire à l’usage des bases de données avec PHP

![Blason Bolton](images/Casa_Bolton.png)

## Rappel : une base de données

Un base de données c’est :

– Un serveur de base de données, auquel on va se connecter (adresse du serveur, login et mot de passe)
– D’une ou plusieurs bases de données, on va devoir une choisir une pour travailler avec
– De tables de données sur lequelles on va éxecuter des requêtes (INSERT, SELECT, UPDATE, DELETE alias CRUD)

## Étape 0 : se connecter au serveur de base de donnnées

    <?php
        $handle = mysqli_connect("localhost","root","monmotdepasse");
    ?>

$handle est le lien à la base de données. On peut en avoir plusieurs liens actifs !

## Étape 1 : travailler avec une base de données

On ajoute un paramètre à la connexion, tout simplement !

    <?php
        $handle = mysqli_connect("localhost","root","monmotdepasse","penduApp");
    ?>


## Étape 2 : réaliser une requête

On fait ça en deux temps (méthode plus clean) : on rédige la requête et puis on l’éxecute

    <?php
        $handle = mysqli_connect("localhost","root","monmotdepasse","penduApp");
        $query = "SELECT * FROM mots";
        $result = mysqli_query($handle,$query);
    ?>

On peut aussi l’éxecuter directement (c’est moins clean, mais plus direct)

    <?php
        $handle = mysqli_connect("localhost","root","monmotdepasse","penduApp");
        $result = mysqli_query("SELECT * FROM mots");
    ?>

$result est un objet (on verra cette notion plus tard) contenant la réponse de la base de données à la requête

## Étape 3 : on exploite le résultat

Un exemple simple, on affiche le nombre de lignes :

    <?php
        $handle = mysqli_connect("localhost","root","monmotdepasse","penduApp");
        $query = "SELECT * FROM mots";
        $result = mysqli_query($handle,$query);
        echo "Il y a " . $result->num_rows . " lignes";
    ?>

### Afficher les résultats

    <?php
        $handle = mysqli_connect("localhost","root","monmotdepasse","penduApp");
        $query = "SELECT * FROM mots";
        $result = mysqli_query($handle,$query);
        while($line = mysqli_fetch_array($result)) {
            echo $line["mot"] . "<br>";
        }
    ?>

* *while* est une structure de contrôle qui s’exécute tant que la condition est *vraie*. **risque de boucles infinies** exemple while(1 == 1)
* **attention** *$line =* un seul égal. C’est une affectation, pas une comparaison !
* *mysqli_fetch_array* est une fonction de PHP qui renvoie une ligne de résultat de la requête SQL à la fois sous forme d’un tableau associatif, jusqu’à ce qu’il n’y ait plus de résultat auquel cas il génère un *false*
Le tableau associatif a pour colonne les colonnes de la table de données : c’est magique !

Donc le while s’éxecute tant que mysqli_fetch_array trouve des lignes et s’arrête quand il n’y en a plus !

