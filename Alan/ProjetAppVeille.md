# Projet veille 2

Lancé le mercredi 31 août 2016

Réaliser une application pour publier sa veille et permettre aux formatteurs de visualiser rapidement qui a proposé quoi et quand  
L’objectif est de permettre aux formatteurs de voir rapidement qui a fait quoi et quand

Les groupes qui réussissent à convaincre le formateur en charge du cours d’ici au vendredi 2 septembre pourront partir immédiatement en week-end

# Cahier des charges

* En trinome imposé
* En PHP pour le backoffice, avec du JS sur le frontoffice
* Avec une documentation d’installation
* Avec un respect des droits d’auteurs
    * Avec un choix de licence
        * Le choix de licence est expliqué
    * Attention si vous reprenez du code d’autres élèves !
* Avec un max de fioritures (visualisation des suppports, stats, graphiques, authentification, intégration slack/twitter/facebook etc.)
    * Vraiment, lâchez vous
        * une fioriture au minimum est requise
* Rédigez votre cahier des charges (ce document étant déjà une partie du cahier des charges)
* Respect du git flow tel que présenté par David (branches feature, branche develop)
* Intégrer le tirage au sort de l’élève le matin même avec du JavaScript
* Framework CSS et JS autorisés
* Héberger l’application sur un serveur personnel ou sur un serveur mutualisé proposé par Pop

# Trinômes

<table>
        <tr><th></th><th>Élève 1</th><th>Élève 2</th><th>Élève 3</th></tr>
        <tr><th>Alan&nbsp;Tannenbaum</th><td>Jimmylan&nbsp;Surqin</td><td>Ingrid&nbsp;Piton</td><td>Nicolas&nbsp;Midavoine</td></tr>
        <tr><th>Bill&nbsp;Joy</th><td>Maxence&nbsp;Dubois</td><td>Aurelie&nbsp;Cliquet</td><td>Leo&nbsp;Riem</td></tr>
        <tr><th>Jimmy&nbsp;Wales</th><td>Amelie&nbsp;Delory</td><td>Pierre&nbsp;Buirette</td><td>Victor&nbsp;Bross</td></tr>
        <tr><th>Richard&nbsp;Stallman</th><td>Kilian&nbsp;Dubois</td><td>Jean-Pierre&nbsp;Annheim</td><td>Selim&nbsp;Farkh</td></tr>
        <tr><th>Steve&nbsp;Wozniak</th><td>Theo&nbsp;Delory</td><td>Remy&nbsp;Blanke</td><td>Younes&nbsp;Labachi</td></tr>
        <tr><th>John&nbsp;Dapper</th><td>Mathilde&nbsp;Voeux</td><td>Bastien&nbsp;Bourgois</td><td>Christopher&nbsp;Delette</td></tr>
        <tr><th>Linus&nbsp;Torvalds</th><td>Camille&nbsp;Woerly</td><td>Lois&nbsp;Marcinkowski</td><td>Jeremy&nbsp;Bustin</td></tr>
</table>
